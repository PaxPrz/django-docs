from django.shortcuts import render, HttpResponse, Http404, get_object_or_404, get_list_or_404, HttpResponseRedirect
from django.template import loader
from django.urls import reverse

from .models import Question, Choice

# Create your views here.
# def index(request):
#     latest_qns = Question.objects.order_by('-pub_date')[:5]
#     out = '<br>'.join(['<a href="/polls/'+str(q.id)+'">'+q.question_text+'</a>' for q in latest_qns])
#     return HttpResponse(out)

def index(req):
    latest_qns = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {'latest_qns':latest_qns}
    return HttpResponse(template.render(context, req))

def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question doesn't exists")
    return render(request, 'polls/detail.html', {'question':question})

def year(request, year):
    year_qns = get_list_or_404(Question, pub_date__year=year)
    print(year_qns)
    return render(request, 'polls/index.html', {'latest_qns':year_qns})

def result(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/result.html', {'question':question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {'question':question, 'error_message':"You didn't select a choice."})
    else:
        print(selected_choice)
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:result', args=(question.id,)))