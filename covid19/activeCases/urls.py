from django.urls import path,re_path
from .views import cases, newCases, recovered

urlpatterns = [
    re_path(r'^[cC]ases{0,1}/', cases, name='cases'),
    re_path(r'^new[cC]ases{0,1}/(\d+)/$', newCases, name='newCases'),
    re_path(r'^r(?:ecovered){0,1}/(?P<cured>\d+)/(?P<total>\d+)/', recovered, name='recovered'),
    # path('recovered/<int:cured>/<int:total>/', recovered, name='recovered'),
]
