from django.shortcuts import render, HttpResponse

# Create your views here.
def cases(request):
    text = ''' <h1> Total confirmed cases are 4,086 </h1>'''
    return HttpResponse(text)

def newCases(request, newAdd):
    print('----------------------')
    print(type(newAdd))
    text = '''<h1> New Cases: {}</h1>'''.format(newAdd)
    print(text)
    return HttpResponse(text)

def recovered(request, cured, total):
    print('------recovered-------')
    print('cured: ',type(cured))
    print('total: ',type(total))
    if isinstance(cured, str):
        cured = int(cured)
        total = int(total)
    text = '''<h1> Total cured: {cured} <br> Total Patient: {total} <br> Percentage: {percentage:.2f}%</h1>'''.format(cured=cured, total=total, percentage=cured*100/total)
    return HttpResponse(text)