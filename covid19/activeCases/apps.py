from django.apps import AppConfig


class ActivecasesConfig(AppConfig):
    name = 'activeCases'
